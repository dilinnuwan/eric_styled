<?php
require_once('admin/function/db_connect.php');

$result = $conn->query("SELECT * FROM blogs");
if ($result->num_rows > 0) {
  // output data of each row

} else {
  $works = array();
}
$conn->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="zh-hk" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title>Style Design Production - Blog</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/animations.css" type="text/css">
  <link rel="stylesheet" href="css/swiper.min.css" type="text/css">

</head>

<body class="current-blog lang-en">

  <div id="main-container">

    <!-- header -->
    <header>

      <a id="header-logo" href="/index_zh.php"></a>

      <!-- nav -->
      <nav>
        <div id="menu-options" class="close">
          <span></span>
          <div class="opened">關閉</div>
          <div class="closed">選單</div>
        </div>

        <ul class="close">
            <li><a href="index_zh.php">首頁</a></li>
            <li>
              <a href="work_residential_zh.php">項目</a>
              <div class="sub">
                <a href="work_residential_zh.php">住宅項目</a>
                <a href="work_retails_zh.php">商業項目</a>
                <a href="work_office_zh.php">辦公室項目</a>
              </div>
            </li>
            <li><a href="about_zh.php">關於</a></li>
            <li><a href="team_zh.php">團隊</a></li>
            <li><a href="blog_zh.php">文章</a></li>
            <li><a href="contact_zh.php">聯絡</a></li>
          </ul>

        <div class="lang-options">
          <a href="blog.php" class="en">En</a> /
          <a href="blog_zh.php" class="chi">中</a>
        </div>
      </nav>

    </header>

    <div class="main-wrapper">
      <div class="main-content">
      <div class="content blog-list">
        <!-- about content -->
        <?php while ($blog = $result->fetch_assoc()) { ?>
          

            <div class="animatedParent animateOnce">
              <div class="blog animated fadeInUpShort">

                <div class="swiper-container s1">
                  <div class="swiper-wrapper">
                    <?php foreach (json_decode($blog['photos']) as $photo) { ?>
                      <div class="swiper-slide"><img src="/photo/blog/<?php echo $photo; ?>"></div>
                    <?php } ?>
                  </div>
                  <div class="pagination"></div>
                </div>

                <div class="article">
                  <h1><?php echo $blog['title_zh']; ?></h1>
                  <p><?php echo nl2br($blog['content_zh']); ?></p>
                  <div class="article-info">
                    <div class="publish-date"><?php 
                    $date = DateTime::createFromFormat('d/m/Y', $blog['date']);
                    echo $date->format('M d, Y'); ?></div><span>|</span>
                      <div class="label"> <?php if ($blog['type'] == 1) { ?>
                          Residential Project
                        <?php } elseif ($blog['type'] == 2) { ?>
                          Retails Project
                        <?php } elseif ($blog['type'] == 3) { ?>
                          Office Project
                        <?php } ?></div>
<!-- <div class="label"><?php echo $blog['type']; ?></div>-->                  
                  </div>
                </div>

              </div>
            </div>
            
          <?php } ?>
          </div>
        <!-- /contact list -->


      </div>
    </div>

    <!-- footer -->
    <div class="gototop"><span></span></div>
    <footer>
      <div class="blockquote"><span></span> / 文章</div>
      <div class="copyright">&copy; 2019 Style Design & Project Ltd.</div>
    </footer>



  </div>

  <script src='js/swiper.min.js' type="text/javascript"></script>
  <script src='js/css3-animate-it.js' type="text/javascript"></script>
  <script src="js/common.js" type="text/javascript"></script>



</body>

</html>