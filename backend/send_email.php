<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

if (isset($_POST['email'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = 'minglee@xtreme-three.com';
    $email_subject = "Style-Design Web Enquiry";
  
    function died($error)
    {
      header("location: contact.php?error=$error");
      die();
    }

    function clean_string($string)
    {
      $bad = array("content-type", "bcc:", "to:", "cc:", "href");
      return str_replace($bad, "", $string);
    }
  
  
    // validation expected data exists
    if (
      !isset($_POST['name']) ||
      !isset($_POST['tel']) ||
      !isset($_POST['email']) ||
      !isset($_POST['type']) ||
      !isset($_POST['message'])
    ) {
      died('We are sorry, but there appears to be a problem with the form you submitted.');
    }

    $name = $_POST['name']; // required
    $tel = $_POST['tel']; // required
    $email = $_POST['email']; // required
    $message = $_POST['message']; // required
    $type= $_POST['type']; // required
  
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  
    if (!preg_match($email_exp, $email)) {
      $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
    }
  
    if (strlen($error_message) > 0) {
      died($error_message);
    }

    $email_message = "Web Enquiry Form details below.\n\n";

    $email_message .= "Name: " . clean_string($name) . "\n";
    $email_message .= "Email: " . clean_string($email) . "\n";
    $email_message .= "Telephone: " . clean_string($tel) . "\n";
    $email_message .= "Type: " . clean_string($type) . "\n";
    $email_message .= "Message: " . clean_string($message) . "\n";
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = '';  // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = '';                     // SMTP username
        $mail->Password   = '';                               // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 587;                                    // TCP port to connect to
    
        //Recipients
        $mail->setFrom($email, clean_string($name));
        $mail->addAddress('info@style-design.com.hk', 'Info Style Design');
        $mail->addReplyTo($email, clean_string($name));
    
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Style-Design Web Enquiry';
        $mail->Body    = nl2br($email_message);
        $mail->AltBody = $email_message;
    
        $mail->send();
        died("Thank you for contacting us. We will be in touch with you very soon.");
    } catch (Exception $e) {
        died("Fail");
    }

}