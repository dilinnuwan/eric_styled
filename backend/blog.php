<?php
require_once('admin/function/db_connect.php');

$result = $conn->query("SELECT * FROM blogs");
if ($result->num_rows > 0) {
  // output data of each row

} else {
  $works = array();
}
$conn->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="zh-hk" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title>Style Design Production - Blog</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/animations.css" type="text/css">
  <link rel="stylesheet" href="css/swiper.min.css" type="text/css">

</head>

<body class="current-blog lang-en">

  <div id="main-container">

    <!-- header -->
    <header>

      <div id="header-logo"></div>

      <!-- nav -->
      <nav>
        <div id="menu-options" class="close">
          <span></span>
          <div class="opened">Close</div>
          <div class="closed">Menu</div>
        </div>

        <ul class="close">
          <li><a href="index.php">Home</a></li>
          <li><a href="work.php">Works</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="team.php">Team</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
        </ul>

        <div class="lang-options">
          <a href="" class="en">En</a> /
          <a href="" class="chi">中</a>
        </div>
      </nav>

    </header>

    <div class="main-wrapper">
      <div class="main-content">

        <!-- about content -->
        <?php while ($blog = $result->fetch_assoc()) { ?>
          <div class="content blog-list">

            <div class="animatedParent animateOnce">
              <div class="blog animated fadeInUpShort">

                <div class="swiper-container s1">
                  <div class="swiper-wrapper">
                    <?php foreach (json_decode($blog['photos']) as $photo) { ?>
                      <div class="swiper-slide"><img src="/photo/blog/<?= $photo ?>"></div>
                    <?php } ?>
                  </div>
                  <div class="pagination"></div>
                </div>

                <div class="article">
                  <h1><?= $blog['title'] ?></h1>
                  <p><?= nl2br($blog['content']) ?></p>
                  <div class="article-info">
                    <div class="publish-date"><?=date('M d, Y', strtotime($blog['date'])) ?></div><span>|</span>
                    <div class="label">Design</div>
                  </div>
                </div>

              </div>
            </div>
          <?php } ?>

        </div>
        <!-- /contact list -->


      </div>
    </div>

    <!-- footer -->
    <div class="gototop"><span></span></div>
    <footer>
      <div class="blockquote"><span></span> / Blog</div>
      <div class="copyright">&copy; 2019 Style Design & Project Ltd.</div>
    </footer>



  </div>

  <script src='js/swiper.min.js' type="text/javascript"></script>
  <script src='js/css3-animate-it.js' type="text/javascript"></script>
  <script src="js/common.js" type="text/javascript"></script>



</body>

</html>