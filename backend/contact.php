
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="zh-hk" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Style Design Production - Contact</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/animations.css" type="text/css">
<link rel="icon" href="http://www.style-design.com.hk/demo/2018/images/ic_favicon.png">

</head>

<body class="current-contact lang-en">

  <div id="main-container">

    <!-- header -->
    <header>

      <div id="header-logo"></div>

      <!-- nav -->
      <nav>
        <div id="menu-options" class="close">
          <span></span>
          <div class="opened">Close</div>
          <div class="closed">Menu</div>
        </div>

        <ul class="close">
          <li><a href="index.php">Home</a></li>
          <li><a href="work.php">Works</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="team.php">Team</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
        </ul>

        <div class="lang-options">
          <a href="" class="en">En</a> / 
          <a href="" class="chi">中</a>
        </div>
      </nav>

    </header>

    <div class="main-wrapper">
      <div class="main-content">

        <!-- about content -->
        <div class="content contact-list">

          <div class="animatedParent animateOnce">
          <div class="lt animated fadeInUpShort">
            <div class="company-info">
              <div class="address">
                Rm C1 1/F<br>
                Wui Wah Factory Bldg.<br>
                41 Wing Hong St.<br>
                Lai Chi Kok<br>
                Kowloon H.K.
              </div>
              <div class="tel">
                <span>T</span>
                +852 9040 5883 <span class="line">—</span> Mr. Nik Chan<br>
                +852 9788 8242 <span class="line">—</span> Mr. Eddy Tam
              </div>
              <div class="email">
                <span>E</span>
                <a href="mailto:info@style-design.com.hk">info@style-design.com.hk</a>
              </div>
            </div>
          </div>
        </div>

          <div class="rt">
            <div class="animatedParent animateOnce">
            <div id="s1" class="section animated fadeInUpShort">
              <form method="POST" action='send_email.php'>
                <input type="text" placeholder="Name" name='name' required>
                <input type="tel" placeholder="Phone" name='tel' required>
                <input type="email" placeholder="Email" name='email' required>
                <select name='type' required>
                  <option value="-">Type of Enquiry</option>
                  <option value="Store Design">Store Design</option>
                  <option value="House Design">House Design</option>
                  <option value="COMMERCIAL">COMMERCIAL</option>
                </select>
                <textarea placeholder="Message" rows="4" name='message' required></textarea>
                
                <button type="submit" class="btn-border">Submit</button>
              </form>
              <?=$_GET['error']==null?"":"<br>".$_GET['error']?>
            </div>
            </div>
          </div>

        </div>
        <!-- /contact list -->


      </div>
    </div>

    <!-- footer -->
    <div class="gototop"><span></span></div>
    <footer>
      <div class="blockquote"><span></span> / Contact</div>
      <div class="copyright">&copy; 2018 Style Design & Project Ltd.</div>
    </footer>



  </div>

<script src='js/css3-animate-it.js' type="text/javascript"></script>
<script src='js/autosize.min.js' type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>

</body>
</html>