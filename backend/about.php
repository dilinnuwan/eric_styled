<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="zh-hk" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Style Design Production - About</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/animations.css" type="text/css">

</head>

<body class="current-about lang-en">

  <div id="main-container">

    <!-- header -->
    <header>

      <div id="header-logo"></div>

      <!-- nav -->
      <nav>
        <div id="menu-options" class="close">
          <span></span>
          <div class="opened">Close</div>
          <div class="closed">Menu</div>
        </div>

        <ul class="close">
          <li><a href="index.php">Home</a></li>
          <li><a href="work.php">Works</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="team.php">Team</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
        </ul>

        <div class="lang-options">
          <a href="" class="en">En</a> / 
          <a href="" class="chi">中</a>
        </div>
      </nav>

    </header>

    <div class="main-wrapper">
      <div class="main-content">

        <!-- about content -->
        <div class="content about-list">

          <div class="lt-fixed">
            <ul class="">
              <li class="current" data-section='s1'><a href="#s1">Background & Service</a></li>
              <li data-section='s2'><a href="#s2">Company Profile</a></li>
              <li data-section='s3'><a href="#s3">Key Customers</a></li>
              <span></span>
            </ul>
          </div>

          <div class="rt">
            <div class="animatedParent animateOnce">
            <div id="s1" class="section animated fadeInUpShort">
                <h1>Background & Service</h1>
                <p>STYLE Design & Project Ltd. was established in 2011 and it was founded by three experienced project managers in retail industry. We offer a one stop services in order to assist client in various stage of project. Ranging from store planning and concept design, design adaptation and development, project management, builder’s contracting works, our professional team in related discipline will offer the genius solution which best fit client’s business strategy.</p>
                <span></span>
            </div>
            </div>
            <div class="animatedParent animateOnce">
            <div id="s2" class="section animated fadeInUpShort">
                <h1>Company Profile</h1>
                <h2>DESIGN TEAM</h2>
                <p>In charge of all design aspects:-planning, concept design, design adaptation, design development, detail design etc.</p>
                <h2>PROJECT TEAM</h2>
                <p>Take care of all the on site and off site project management services, budget estimation, programme, landlord submission and coordination, site coordination, site monitoring etc.</p>
                <h2>CONSTRUCTION</h2>
                <p>Our professional team will provide on site supervision and site construction works</p>
                <span></span>
            </div>
            </div>
            <div class="animatedParent animateOnce">
            <div id='s3'class="section animated fadeInUpShort">
                <h1>Key Customers</h1>
                <ul>
                  <li><h3>Shiseido</h3></li>
                  <li><h3>IPSA</h3></li>
                  <li><h3>Dior</h3></li>
                  <li><h3>Bossini</h3></li>
                  <li><h3>O’fee</h3></li>
                  <li><h3>Kwanpen</h3></li>
                  <li><h3>KPNY</h3></li>
                  <li><h3>Amika</h3></li>
                  <li><h3>Private Shop</h3></li>
                </ul>
                <span></span>
            </div>
            </div>
          </div>

        </div>
        <!-- /about list -->


      </div>
    </div>

    <!-- footer -->
    <div class="gototop"><span></span></div>
    <footer>
      <div class="blockquote"><span></span> / About</div>
      <div class="copyright">&copy; 2018 Style Design & Project Ltd.</div>
    </footer>



  </div>

<script src='js/css3-animate-it.js' type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>


</body>
</html>