$(function(){

  /* mobile? */
  var $mobile = 
  /ipad|iphone|ipod|android|blackberry|webos|windows phone/i.test(navigator.userAgent.toLowerCase());

  if($mobile)
  {
     $('body').addClass('InMobile');
  }
  else
  {
     $('body').addClass('InDesktop');
  }

  
  $('#menu-options').click(function(){
  /* Menu (Desktop) */
    if(!$mobile){
      if($(this).hasClass('open')){
        $(this).addClass('close');
        $(this).removeClass('open');
        $('body, nav, nav ul').addClass('close');
        $('body, nav, nav ul').removeClass('open');
      }else{
        $(this).addClass('open');
        $(this).removeClass('close');
        $('body, nav, nav ul').addClass('open');
        $('body, nav, nav ul').removeClass('close');
      }
    }else{
  /* Menu (Mobile) */
      if($(this).hasClass('open')){
        $(this).addClass('close');
        $(this).removeClass('open');
        $('body, nav, nav ul').addClass('close');
        $('body, nav, nav ul').removeClass('open');
      }else{
        $(this).addClass('open');
        $(this).removeClass('close');
        $('body, nav, nav ul').addClass('open');
        $('body, nav, nav ul').removeClass('close');
      }
    }

  });


  /* Home */
  if($('body').hasClass('current-home')){

    $('div[data-work="p1"]').addClass('show');
    // start animation
    $('body').width();

    $('body').addClass('start-animate');
    setTimeout(function(){$('.number span').shuffleLetters()},300);

    // show project
    var $projectlength = $('.txtcontent .title span').length,
    $projectindex = 0;

    $('.arrow .prev').click(function(){
      if($projectindex > 0){

        $projectindex--;
        showproject($projectindex);

        if($projectindex == 0){
          $('.arrow .prev').addClass('disable');
        }else{
          $('.arrow .prev').removeClass('disable');
        }

        $('.arrow .next').removeClass('disable');
      }
    });
    $('.arrow .next').click(function(){
      if($projectindex < $projectlength - 1){

        $projectindex++;
        showproject($projectindex);
        if($projectindex == $projectlength - 1){
          $('.arrow .next').addClass('disable');
        }else{
          $('.arrow .next').removeClass('disable');
        }

        $('.arrow .prev').removeClass('disable');
      }
    });

    function showproject(index){

      //show which project
      $('div[data-work^="p"]').removeClass('show').width();
      $('div[data-work="p'+ (index+1) +'"]').addClass('show');

      //do action
      /*$('.current-home').removeClass('start-animate2');

      $('.current-home').offsetWidth;
      $('.current-home').addClass('start-animate2');*/

      setTimeout(function(){$('.number span').shuffleLetters()},100);
    }

  }

  /* About page, scroll appear */
  if($('body').hasClass('current-about') || $('body').hasClass('current-team')){

    var lastId, 
    topMenuHeight = 0,
    menuItems = $(".lt-fixed").find('a'),
    scrollItems = menuItems.map(function(){
       var item = $($(this).attr("href"));
        if (item.length) { return item; }
     });

    // Bind to scroll
    $(window).scroll(function(){
      checkscroll();        
    });
    checkscroll();

    function checkscroll(){
     // Get container scroll position

     var fromTop = $(this).scrollTop()+topMenuHeight;

     var mobileouter = 2;
      if($mobile && $(window).width() > 1025){ 
        var mobileouter = 3;
      }else if($mobile && $(window).width() < 1025){
        var mobileouter = 2;
      }

     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top - $(window).outerHeight() + ($(window).outerHeight() / mobileouter) < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";


    if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems
           .parent().removeClass("current")
           .end().filter("[href='#"+id+"']").parent().addClass("current");
      }  
     } 


    $(".lt-fixed a").click(function(e){
      e.preventDefault();
      var to = $(this).attr("href");

      var mobileoffset = 0;
      if($mobile){ var mobileoffset = 30;}else{}

      /* done the fadein css3-animate-it */
      var body = $("html, body");

      if($(to).hasClass('go')){
        
        body.stop().animate({scrollTop: $(to).offset().top - 45 - mobileoffset }, 500, 'swing');

      }else{

        body.stop().animate({scrollTop: $(to).offset().top - 85 - mobileoffset }, 500, 'swing'); /* +40 */

      }

      /* if option menu opened */
      if($('.lt-fixed').hasClass('open')){
        $('.lt-fixed').toggleClass('open');
      }
    });


    /* option menu (mobile) */
    if($mobile){
      $('.lt-fixed ul span').click(function(){
        $('.lt-fixed').toggleClass('open');
      });
    }

  }



  /* Contact */
  if($('body').hasClass('current-contact')){

    $('.contact-list select').change(function() {
      $( ".contact-list select option:selected" ).each(function() {
        $('.contact-list select').css('color','white');
        console.log('w');
      });
    });

    autosize($('.contact-list textarea'));
  }



  /* Blog */
  if($('body').hasClass('current-blog')){

    $('.swiper-container').each(function(){
          new Swiper($(this), {
              pagination: {
                    el: $(this).find('.pagination'),
                    clickable: true
              },
              autoplay: {
                disableOnInteraction: false
              },
              loop: true
          });
       });

  }

  /* Work */
    if($('body').hasClass('current-works')){

    if($(window).width() > 767){


    $('.imgs').each(function(){
      $(this).find('.img').eq(0).addClass('show');
    });

    $('.arrow .next').click(function(){

      var $imglength = $(this).closest('.work').find('.imgs .img').length,
      $imgindex = $(this).closest('.work').find('.imgs .img.show').index();

      if($imgindex != $imglength - 1){
        $imgindex ++;
          $(this).closest('.work').find('.imgs .img').removeClass('show');
          $(this).closest('.work').find('.imgs .img').eq($imgindex).addClass('show');
          $(this).closest('.work').find('.prev').removeClass('disable');
          if($imgindex == $imglength - 1){$(this).closest('.work').find('.next').addClass('disable');}
      }
    });
    $('.arrow .prev').click(function(){
      var $imglength = $(this).closest('.work').find('.imgs .img').length,
      $imgindex = $(this).closest('.work').find('.imgs .img.show').index();

      if($imgindex > 0){
        $imgindex --;
          $(this).closest('.work').find('.imgs .img').removeClass('show');
          $(this).closest('.work').find('.imgs .img').eq($imgindex).addClass('show');
          $(this).closest('.work').find('.next').removeClass('disable');
          if($imgindex == 0){$(this).closest('.work').find('.prev').addClass('disable');}
      }
    });


    //random text with scroll effect
    //=======================
    var getElementsInArea = (function(docElm){
    var viewportHeight = docElm.clientHeight;

    return function(e, opts){
        var found = [], i;
        
        if( e && e.type == 'resize' )
            viewportHeight = docElm.clientHeight;

        for( i = opts.elements.length; i--; ){
            var elm        = opts.elements[i],
                pos        = elm.getBoundingClientRect(),
                topPerc    = pos.top    / viewportHeight * 100,
                bottomPerc = pos.bottom / viewportHeight * 100,
                middle     = (topPerc + bottomPerc)/2,
                inViewport = middle > opts.zone[1] && 
                             middle < (100-opts.zone[1]);

            elm.classList.toggle(opts.markedClass, inViewport);

            $('.ruler .line').removeClass(opts.markedClass);
 
            if($('.w.'+ opts.markedClass).index() != -1){
              $('.ruler .line').eq( $('.w.'+ opts.markedClass).index() ).addClass(opts.markedClass);
            };
            
            if(!$('.w.'+ opts.markedClass).hasClass('done')){
              setTimeout(function(){$('.w.'+ opts.markedClass +' .number span').shuffleLetters()},590);
              $('.w.'+ opts.markedClass).addClass('done');
            }

            if( inViewport )
                found.push(elm);
            }
        };


    })(document.documentElement);


    ////////////////////////////////////

    window.addEventListener('scroll', f)
    window.addEventListener('resize', f)

    function f(e){
        getElementsInArea(e, {
            elements    : document.querySelectorAll('div.w'), 
            markedClass : 'highlight--1',
            zone        : [0, 20] // percentage distance from top & bottom
        });

        /*getElementsInArea(e, {
            elements    : document.querySelectorAll('div.w'), 
            markedClass : 'highlight--2',
            zone        : [40, 40] // percentage distance from top & bottom
        });*/
    }
        //=======================
    $(window).scrollTop(1).stop().scrollTop(0);

    //scrollbar
    var $worklength = $('.w').length,
    $lineindex = 0;

    for(var i = 1; i < $worklength + 1; i++){

      if(i < 10){
        $lineindex = '0' + i;
      }else{
        $lineindex = i;
      }

      $('.ruler .lines').append('<div class="line"><span>'+ $lineindex +'</span></div>');
    }
      var body = $("html, body");

      $('.ruler .arrow-icon .top').click(function(){
        var $windex = $('.w').index($('.w.highlight--1'));
        if( $windex != 0){
          body.stop().animate({scrollTop: $('.w').eq($windex - 1).offset().top - 100  }, 500, 'swing');
        }
      });
      $('.ruler .arrow-icon .down').click(function(){
        var $windex = $('.w').index($('.w.highlight--1'));
        if( $windex != $('.w').length - 1){
          body.stop().animate({scrollTop: $('.w').eq($windex + 1).offset().top - 100  }, 500, 'swing');
        }
      });

  }else{ 
    //mobile

      $(".swiper-wrapper img").each(function(i, elem) {
        var img = $(elem);
        var div = $("<div />").css({
          "background": "url(" + img.attr("src") + ") center center no-repeat",
          "background-size": "cover",
          width:'100%',
          height:'100%'
        });
        img.replaceWith(div);
      });


      $('.swiper-container').each(function(){
          new Swiper($(this), {
            setWrapperSize:true,
              pagination: {
                    el: $(this).find('.pagination'),
                    clickable: true
              },
              autoplay: {
                disableOnInteraction: false
              },
              loop: true
          });
       });
    }
  }//work


 // $('.number span').shuffleLetters();
 var body = $("html, body");
 $('.gototop').click(function(){
  body.stop().animate({scrollTop: 0}, 500, 'swing');
 });
});