<?php 

ob_start();
session_start();

$msg = '';

if (!isset($_SESSION['login_user'])) {
    session_unset();
    session_destroy();
    $msg = '登入超時，請重新登入！';
    header("Location: ../../login.php?msg=".$msg);
    die();
}