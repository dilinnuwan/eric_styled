<?php

include 'auth/authCheck.php';
require_once('db_connect.php');

$action = $_POST['action'];
$type = $_POST['type'];
if ($action && $type) {
    if ($action == 'create') {
        if ($type == 'work') {
            $stmt = $conn->prepare("INSERT INTO works (title, type, content,place,photos) VALUES (?, ?, ?, ?, ?)");
            $stmt->bind_param("sssss", $title, $type_u, $content, $place, $photos);

            $title = $_POST['title'];
            $type_u = $_POST['type_u'];
            $content = $_POST['content'];
            $place = $_POST['place'];
            if ($title && $type_u && $content && $place) {
                $photos = array();
                if (file_exists($_FILES['photo_1']['tmp_name'])) {
                    $photos[0] = uploadPhotos($_FILES["photo_1"], $type, true)['result'];
                }
                if (file_exists($_FILES['photo_2']['tmp_name'])) {
                    $photos[1] = uploadPhotos($_FILES["photo_2"], $type, true)['result'];
                }
                if (file_exists($_FILES['photo_3']['tmp_name'])) {
                    $photos[2] = uploadPhotos($_FILES["photo_3"], $type, true)['result'];
                }
                if (file_exists($_FILES['photo_4']['tmp_name'])) {
                    $photos[3] =  uploadPhotos($_FILES["photo_4"], $type, true)['result'];
                }
                if (file_exists($_FILES['photo_5']['tmp_name'])) {
                    $photos[4] =  uploadPhotos($_FILES["photo_5"], $type, true)['result'];
                }
                if (sizeof($photos) < 5) {
                    $msg = 'Error: Not enough Photo (Require 5 photos)';
                    header("location: ../admin.php?msg=" . $msg);
                } else {
                    $photos = json_encode($photos);
                    $stmt->execute();
                    $msg = 'Add data successful';
                    header("location: ../admin.php?msg=" . $msg);
                    die();
                }
            } else {
                header("location: ../login.php");
                die();
            }
        } else if ($type == 'blog') {
            $stmt = $conn->prepare("INSERT INTO blogs (title, type, content,date,photos) VALUES (?, ?, ?, ?, ?)");
            $stmt->bind_param("sssss", $title, $type_u, $content, $date, $photos);

            $title = $_POST['title'];
            $type_u = $_POST['type_u'];
            $content = $_POST['content'];
            $date = $_POST['date'];
            if ($title && $type_u && $content && $date) {
                $photos = array();
                if (file_exists($_FILES['photo_1']['tmp_name'])) {
                    $photos[0] = uploadPhotos($_FILES["photo_1"], $type, true)['result'];
                }
                if (file_exists($_FILES['photo_2']['tmp_name'])) {
                    $photos[1] = uploadPhotos($_FILES["photo_2"], $type, true)['result'];
                }
                if (file_exists($_FILES['photo_3']['tmp_name'])) {
                    $photos[2] = uploadPhotos($_FILES["photo_3"], $type, true)['result'];
                }
                if (file_exists($_FILES['photo_4']['tmp_name'])) {
                    $photos[3] =  uploadPhotos($_FILES["photo_4"], $type, true)['result'];
                }
                if (file_exists($_FILES['photo_5']['tmp_name'])) {
                    $photos[4] =  uploadPhotos($_FILES["photo_5"], $type, true)['result'];
                }
                if (sizeof($photos) < 5) {
                    $msg = 'Error: Not enough Photo (Require 5 photos)';
                    header("location: ../add-blog.php?msg=" . $msg);
                } else {
                    $photos = json_encode($photos);
                    $stmt->execute();
                    $msg = 'Add data successful';
                    header("location: ../add-blog.php?msg=" . $msg);
                    die();
                }
            } else {
                header("location: ../login.php");
                die();
            }
        } else {
            header("location: ../login.php");
            die();
        }
    } else if ($action == 'edit') {
        $id = $_POST['id'];
        if (!isset($id)) {
            echo 1;
            header("location: login.php");
        }
        if ($type == 'work') {
            $stmt = $conn->prepare("UPDATE works SET title=? ,type=?, content=?, place=?  WHERE id=?");
            $stmt->bind_param("ssssi", $title, $type_u, $content, $place, $id);

            $title = $_POST['title'];
            $type_u = $_POST['type_u'];
            $content = $_POST['content'];
            $place = $_POST['place'];
            if ($title && $type_u && $content && $place) {
                $stmt->execute();
                $stmt->close();
            } else {
                echo 2;
                header("location: ../login.php");
            }

            $result = $conn->query("SELECT photos FROM works WHERE id =" . $id);
            if ($result->num_rows > 0) {
                $photos = $result->fetch_assoc();
            }

            $stmt = $conn->prepare("UPDATE works SET photos = ? WHERE id = ?");
            $stmt->bind_param("si", $photos, $id);

            $photos = json_decode($photos['photos']);
            if (file_exists($_FILES['photo_1']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos[0]);
                $photos[0] = uploadPhotos($_FILES["photo_1"], $type, true)['result'];
            }
            if (file_exists($_FILES['photo_2']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos1);
                $photos[1] = uploadPhotos($_FILES["photo_2"], $type, true)['result'];
            }
            if (file_exists($_FILES['photo_3']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos[2]);
                $photos[2] = uploadPhotos($_FILES["photo_3"], $type, true)['result'];
            }
            if (file_exists($_FILES['photo_4']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos[3]);
                $photos[3] =  uploadPhotos($_FILES["photo_4"], $type, true)['result'];
            }
            if (file_exists($_FILES['photo_5']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos[4]);
                $photos[4] =  uploadPhotos($_FILES["photo_5"], $type, true)['result'];
            }

            $photos = json_encode($photos);
            $stmt->execute();
            $stmt->close();

            $msg = "Edit data id:$id Successful";
            header("location: ../admin.php?msg=" . $msg);
            die();
        } else if ($type == 'blog') {
            $stmt = $conn->prepare("UPDATE blogs SET title=? ,type=?, content=?, date=?  WHERE id=?");
            $stmt->bind_param("ssssi", $title, $type_u, $content, $date, $id);

            $title = $_POST['title'];
            $type_u = $_POST['type_u'];
            $content = $_POST['content'];
            $date = $_POST['date'];
            if ($title && $type_u && $content && $date) {
                $stmt->execute();
            } else {
                header("location: login.php");
                die();
            }

            $result = $conn->query("SELECT photos FROM blogs WHERE id =" . $id);
            if ($result->num_rows > 0) {
                $photos = $result->fetch_assoc();
            }

            $stmt = $conn->prepare("UPDATE blogs SET photos = ? WHERE id = ?");
            $stmt->bind_param("si", $photos, $id);

            $photos = json_decode($photos['photos']);
            if (file_exists($_FILES['photo_1']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos[0]);
                $photos[0] = uploadPhotos($_FILES["photo_1"], $type, true)['result'];
            }
            if (file_exists($_FILES['photo_2']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos1);
                $photos[1] = uploadPhotos($_FILES["photo_2"], $type, true)['result'];
            }
            if (file_exists($_FILES['photo_3']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos[2]);
                $photos[2] = uploadPhotos($_FILES["photo_3"], $type, true)['result'];
            }
            if (file_exists($_FILES['photo_4']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos[3]);
                $photos[3] =  uploadPhotos($_FILES["photo_4"], $type, true)['result'];
            }
            if (file_exists($_FILES['photo_5']['tmp_name'])) {
                unlink("../../photo/" . $type_u . "/" . $photos[4]);
                $photos[4] =  uploadPhotos($_FILES["photo_5"], $type, true)['result'];
            }

            $photos = json_encode($photos);
            $stmt->execute();
            $stmt->close();

            $msg = "Edit data id:$id Successful";
            header("location: ../add-blog.php?msg=" . $msg);
            die();
        } else {
            header("location: ../login.php");
            die();
        }
    } else if ($action == 'delete') {
        $id = $_POST['id'];
        if (!isset($id)) {
            echo 1;
            header("location: ../login.php");
        }
        if ($type == 'work') {
            $stmt = $conn->prepare("DELETE FROM works WHERE id=?");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->close();

            $msg = "Delete data id:$id Successful";
            header("location: ../admin.php?msg=" . $msg);
            die();
        } else if ($type == 'blog') {
            $stmt = $conn->prepare("DELETE FROM blogs WHERE id=?");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->close();

            $msg = "Delete data id:$id Successful";
            header("location: ../add-blog.php?msg=" . $msg);
            die();
        } else {
            header("location: ../login.php");
            die();
        }
    } else {
        header("location: ../login.php");
        die();
    }
} else {
    header("location: ../login.php");
    die();
}


function uploadPhotos($photo, $type, $one = false)
{
    $result = array();
    $error = array();
    $extension =  array("jpeg", "jpg", "png", "gif", "bmp");
    $file_name = $photo["name"];
    $file_tmp = $photo["tmp_name"];
    $ext = pathinfo($file_name, PATHINFO_EXTENSION);

    if (in_array($ext, $extension)) {
        if (!file_exists("../../photo/" . $type . "/" . $file_name)) {
            array_push($result, $file_name);
            move_uploaded_file($file_tmp, "../../photo/" . $type . "/" . $file_name);
        } else {
            $filename = basename($file_name, $ext);
            $newFileName = $filename . time() . "." . $ext;
            array_push($result, $newFileName);
            move_uploaded_file($file_tmp, "../../photo/" . $type . "/" . $newFileName);
        }
    } else {
        array_push($error, "$file_name, ");
    }

    if ($one) {
        return ['result' => $result[0], 'error' => $error];
    } else {
        return ['result' => $result, 'error' => $error];
    }
}
