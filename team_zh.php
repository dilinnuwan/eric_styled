<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="zh-hk" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Style Design Production - Team</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/style.css" type="text/css">

<!-- Team's page only -->
<link rel="stylesheet" href="css/animations.css" type="text/css">

</head>

<body class="current-team lang-en">

  <div id="main-container">

    <!-- header -->
    <header>

      <a id="header-logo" href="/index_zh.php"></a>

      <!-- nav -->
      <nav>
        <div id="menu-options" class="close">
          <span></span>
          <div class="opened">關閉</div>
          <div class="closed">選單</div>
        </div>

        <ul class="close">
            <li><a href="index_zh.php">首頁</a></li>
            <li>
              <a href="work_residential_zh.php">項目</a>
              <div class="sub">
                <a href="work_residential_zh.php">住宅項目</a>
                <a href="work_retails_zh.php">商業項目</a>
                <a href="work_office_zh.php">辦公室項目</a>
              </div>
            </li>
            <li><a href="about_zh.php">關於</a></li>
            <li><a href="team_zh.php">團隊</a></li>
            <li><a href="blog_zh.php">文章</a></li>
            <li><a href="contact_zh.php">聯絡</a></li>
          </ul>

        <div class="lang-options">
          <a href="team.php" class="en">En</a> / 
          <a href="team_zh.php" class="chi">中</a>
        </div>
      </nav>

    </header>

    <div class="main-wrapper">
      <div class="main-content">

        <!-- about content -->
        <div class="content team-list">

          <div class="lt-fixed">
            <ul class="">
              <li class="current" data-section='s1'><a href="#s1">組織結構</a></li>
              <li data-section='s2'><a href="#s2">團隊成員</a></li>
              <span></span>
            </ul>
          </div>

          <div class="rt">
            <div class="animatedParent animateOnce">
            <div id="s1" class="section animated fadeInUpShort">
                <h1>組織結構</h1>
                

                <div class="o-chart">

                  <div class="big-dot">
                    <div class="companyname">意念設計有限公司</div>
                    <span></span>
                  </div>

                  <div class="small-dot">
                    <div class="dept-head">
                      <div class="title">設計及項目經理</div>
                      <div class="member-name">陳健威</div>
                      <span></span>
                    </div>
                    <div class="dept-member">
                      <div class="title">設計師</div>
                      <span></span>
                    </div>
                    <div class="dept-member">
                      <div class="title">設計助理</div>
                      <span></span>
                    </div>
                  </div>

                  <div class="small-dot">
                    <div class="dept-head">
                      <div class="title">項目經理</div>
                      <div class="member-name">譚俊文</div>
                      <span></span>
                    </div>
                    <div class="dept-member">
                      <div class="title">工地監督</div>
                      <span></span>
                    </div>
                    <div class="dept-member">
                      <div class="title">技術人員</div>
                      <span></span>
                      
                    </div>
                  </div>

                  


                </div>

            </div>
            </div>
            <div class="animatedParent animateOnce">
            <div id="s2" class="section animated fadeInUpShort">
                <h1>團隊成員</h1>
                
                

                <div class="member">
                  <div class="profile-img rotate180">
                    <img src="images/profile03.png">
                    <span class="border"></span>
                  </div>
                  <div class="name">
                   陳健威, Nik
                  </div>
                  <div class="title">
                    設計及項目經理
                  </div>
                  <ul class="point">
                    多年室內設計及設計管理經驗, 早年專注於商業設計及管理,善於將品牌概念呈現於室內設計上。及後設計範圍擴展到住宅項目上, 重視空間規劃及物料配合。崇尚自然﹑簡約的設計,追求設計上「美」與「風格」的配合。

                  </ul>
                </div>

                <div class="member">
                  
                  <div class="name">
                    譚俊文, Eddy
                  </div>
                  <div class="title">
                    項目經理

                  </div>
                  <ul class="point">
                    於理工大學建築及房地產學系畢業，並擁有超過十年項目管理經驗。能有效監察項目進度、工程質量及預算控制，使項目能因應客人要求順利完成。

                  </ul>
                </div>
            </div>
            </div>

          </div>

        </div>
        <!-- /Team list -->


      </div>
    </div>

    <!-- footer -->
    <div class="gototop"><span></span></div>
    <footer>
      <div class="blockquote"><span></span> / 團隊</div>
      <div class="copyright">&copy; 2019 Style Design & Project Ltd.</div>
    </footer>



  </div>

<script src='js/css3-animate-it.js' type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>


</body>
</html>