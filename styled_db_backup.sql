-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2020 at 03:53 AM
-- Server version: 5.1.61
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `styled`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', '21f62beff79277ead3248f9ca8675c81');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title_zh` varchar(25) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `priority` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `content_zh` text NOT NULL,
  `date` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `photos` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `title_zh`, `type`, `priority`, `content`, `content_zh`, `date`, `photos`) VALUES
(1, 'Testing title', 'ä¸­æ–‡ Title', 3, 0, 'This is the key content!', 'å‘¢åº¦ä¸€å•²å…§å®¹ï¼', '29/08/2020', '["avatar head.jpeg"]'),
(2, 'Yuen Long', 'Yuen Long', 1, 0, 'test', 'test', '28/08/2020', '["Living Room View 01 (R2).jpg"]');

-- --------------------------------------------------------

--
-- Table structure for table `works`
--

CREATE TABLE IF NOT EXISTS `works` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title_zh` varchar(25) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `priority` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `content_zh` text NOT NULL,
  `place` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `place_zh` text NOT NULL,
  `photos` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `works`
--

INSERT INTO `works` (`id`, `title`, `title_zh`, `type`, `priority`, `content`, `content_zh`, `place`, `place_zh`, `photos`) VALUES
(17, 'Shenzhen å¤©å¾¡è±ªåº­', 'å¤©å¾¡è±ªåº­', 1, 0, 'Design Project\r\n\r\nArea: 3,390 sq.ft.', 'Design Project\r\n\r\nArea: 3,390 sq.ft.', 'Baoan Qu, Shenzhen, Guangdong Sheng', 'Baoan Qu, Shenzhen, Guangdong Sheng ', '["Cam012 D.jpg","Cam011.jpg","Master 03.jpg","KELLY TOILET Cam02b.jpg","Cam 002.jpg"]'),
(18, 'Shenzhen é›²ç’½éŒ¦åº­', '', 1, 0, 'Design Project\r\n\r\nArea: 12,540 sq.ft.', '', 'Baoan Qu, Shenzhen Shi, Guangdong Sheng', '', '["5.jpg","2.jpg","6.jpg","4.jpg","1.jpg"]'),
(19, 'Hong Lok Yuen', '', 1, 0, 'Design Project\r\n\r\nArea: 3,200 sq.ft.', '', 'Tai Po, Hong Kong', '', '["L Cam 07.jpg","Masterbedroom 01a.jpg","L Cam 01.jpg","Toilet01ps.jpg","WhatsApp Image 2018-04-29 at 12.10.22.jpeg"]'),
(20, 'Jade Terrace', '', 1, 0, 'Design & Build Project\r\n\r\nArea: 827 sq.ft.', '', 'Causeway Bay, Hong Kong', '', '["2017-09-28-PHOTO-00000032.jpg","2017-09-28-PHOTO-00000031.jpg","2017-09-28-PHOTO-00000030.jpg","2017-09-28-PHOTO-00000033.jpg","2017-09-28-PHOTO-00000034.jpg"]'),
(21, 'Breezy Mansion', '', 1, 0, 'Design Project\r\n\r\nArea: 490 sq.ft.', '', 'Bonham Road, Hong Kong', '', '["IMG_2325.jpg","IMG_2310.jpg","IMG_2315.jpg"]'),
(22, 'Jubilee Garden', '', 1, 0, 'Design & Build Project\r\n\r\nArea: 653 sq.ft.', '', 'Fotan, Hong Kong', '', '["PHOTO-2019-06-03-22-23-48.jpg","PHOTO-2019-06-03-22-23-48 (2).jpg","PHOTO-2019-06-03-22-23-48 (1).jpg"]'),
(23, 'Chi Fu Fa Yuen', '', 1, 0, 'Design & Build Project\r\n\r\nArea: 439 sq.ft.', '', 'Chi Fu, Hong Kong', '', '["2.1559640563.jpg","1.1559640563.jpg","3.1559640563.jpg","4.1559639595.jpg"]'),
(24, 'Yu Fai Court', '', 1, 0, 'Design & Build Project\r\n\r\nArea: 384 sq.ft.', '', 'Aberdeen, Hong Kong', '', '["e.jpg","d.jpg","c.jpg","a.jpg","b.jpg"]'),
(25, 'Mount Pavilia', '', 1, 0, 'Design & Build Project\r\n\r\n968 sq.ft.', '', 'Clear Water Bay Road, Hong Kong', '', '["k.jpg","g.jpg","j.jpg","h.jpg","f.jpg"]'),
(29, 'Hello World 1', 'hello', 1, 10, 'test 1', 'test 2', 'DT Toronto', 'DT', '["things-to-do-in-toronto-part-1-256x256.jpg"]');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
