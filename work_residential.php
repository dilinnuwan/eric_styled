<?php
require_once('admin/function/db_connect.php');

$result = $conn->query("SELECT * FROM works WHERE type = 1 ORDER BY priority DESC, id DESC");
$num_rows = $result->num_rows;
if ($num_rows > 0) {
  // output data of each row

} else {
  $works = array();
}
$conn->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="zh-hk" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title>Style Design Production - Work</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/animations.css" type="text/css">
  <link rel="stylesheet" href="css/swiper.min.css" type="text/css">

</head>

<body class="current-works lang-en">

  <div id="main-container">

    <!-- header -->
    <header>

      <a id="header-logo" href="/"></a>

      <!-- nav -->
      <nav>
        <div id="menu-options" class="close">
          <span></span>
          <div class="opened">Close</div>
          <div class="closed">Menu</div>
        </div>

        <ul class="close">
          <li><a href="index.php">Home</a></li>
          <li>
            <a href="work_residential.php">Works</a>
            <div class="sub">
                <a href="work_residential.php" class="current">Residential Project</a>
                <a href="work_retails.php">Retails Project</a>
                <a href="work_office.php">Office Project</a>
            </div>
          </li>
          <li><a href="about.php">About</a></li>
          <li><a href="team.php">Team</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
        </ul>

        <div class="lang-options">
          <a href="work_residential.php" class="en">En</a> /
          <a href="work_residential_zh.php" class="chi">中</a>
        </div>
      </nav>

    </header>

    <div class="main-wrapper">
      <div class="main-content">

        <!-- about content -->
        <div class="content work-list">

          <div class="lt">
            <?php $i = 1;
            while ($work = $result->fetch_assoc()) { ?>
              <!-- work -->
              <div class="w">
                <div class="work">

                  <!-- work-content -->
                  <div class="work-content">

                    <div class="txtcontent">
                      <div class="arrow">
                        <div class="prev disable"></div>
                        <div class="next"></div>
                      </div>
                      <div class="type"> <?php if ($work['type'] == 1) { ?>
                          Residential Project
                        <?php } elseif ($work['type'] == 2) { ?>
                          Retails Project
                        <?php } elseif ($work['type'] == 3) { ?>
                          Office Project
                        <?php } ?></div>
                      <h1 class="title"><?php echo $work['title']; ?></h1>
                      <p class="description">
                        <?php echo nl2br($work['content']); ?>
                      </p>
                    </div>

                    <div class="thumbnail">
                      <div class="number"><span> <?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></span></div>
                      <div class="imgs swiper-container">
                        <div class="swiper-wrapper">
                          <?php foreach (json_decode($work['photos']) as $photo) { ?>
                            <div class="img swiper-slide"><img src="/photo/work/<?php echo $photo; ?>"></div>
                          <?php } ?>
                        </div>
                        <div class="pagination"></div>
                      </div>
                      <div class="location"><?php echo strtoupper($work['place']); ?></div>
                    </div>

                  </div>
                  <!-- /work-content -->
                </div>
              </div>
            <?php $i++;} ?>
            
            
            <?php if($num_rows<2){ ?>
            <div>
              <div class="w ">
                <div class="work">
                  <!-- work-content -->
                  <div class="work-content">
                  </div>
                  <!-- /work-content -->
                </div>
              </div>
            </div>
            <?php } ?>


          </div>

          <div class="rt">
            <div class="ruler">
              <div class="arrow-icon">
                <div class="top"><span></span></div>
              </div>
              <div class="lines">
                <!-- gen from common.js //scrollbar -->
              </div>
              <div class="arrow-icon">
                <div class="down"><span></span></div>
              </div>
            </div>
          </div>


        </div>
        <!-- /works list -->


      </div>
    </div>


    <!-- footer -->
    <div class="gototop"><span></span></div>
    <footer>
      <div class="blockquote"><span></span> / Works</div>
      <div class="copyright">&copy; 2019 Style Design & Project Ltd.</div>
    </footer>



  </div>

  <script src='js/swiper.min.js' type="text/javascript"></script>
  <script src='js/css3-animate-it.js' type="text/javascript"></script>
  <script src='js/jquery.shuffleLetters.js' type="text/javascript"></script>
  <script src="js/common.js" type="text/javascript"></script>



</body>

</html>