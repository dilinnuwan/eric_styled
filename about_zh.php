<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="zh-hk" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Style Design Production - About</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/animations.css" type="text/css">

</head>

<body class="current-about lang-en">

  <div id="main-container">

    <!-- header -->
    <header>

      <a id="header-logo" href="/index_zh.php"></a>

      <!-- nav -->
      <nav>
        <div id="menu-options" class="close">
          <span></span>
          <div class="opened">關閉</div>
          <div class="closed">選單</div>
        </div>

        <ul class="close">
            <li><a href="index_zh.php">首頁</a></li>
            <li>
              <a href="work_residential_zh.php">項目</a>
              <div class="sub">
                <a href="work_residential_zh.php">住宅項目</a>
                <a href="work_retails_zh.php">商業項目</a>
                <a href="work_office_zh.php">辦公室項目</a>
              </div>
            </li>
            <li><a href="about_zh.php">關於</a></li>
            <li><a href="team_zh.php">團隊</a></li>
            <li><a href="blog_zh.php">文章</a></li>
            <li><a href="contact_zh.php">聯絡</a></li>
          </ul>

        <div class="lang-options">
          <a href="about.php" class="en">En</a> / 
          <a href="about_zh.php" class="chi">中</a>
        </div>
      </nav>

    </header>

    <div class="main-wrapper">
      <div class="main-content">

        <!-- about content -->
        <div class="content about-list">

          <div class="lt-fixed">
            <ul class="">
              <li class="current" data-section='s1'><a href="#s1">背景與服務</a></li>
              <li data-section='s2'><a href="#s2">公司簡介</a></li>
              
              <span></span>
            </ul>
          </div>

          <div class="rt">
            <div class="animatedParent animateOnce">
            <div id="s1" class="section animated fadeInUpShort">
                <h1>背景與服務</h1>
                <p>意念設計有限公司由兩位具經的設計及項目經理於2011年創辦,向客戶提供一站式室內設計及工程服務。由規劃、設計、項目管理，我們專業的團隊能為客戶於各領域上提供專業及合適的選項及建議，令客戶得到真正所需。</p>
                <span></span>
            </div>
            </div>
            <div class="animatedParent animateOnce">
            <div id="s2" class="section animated fadeInUpShort">
                <h1>公司簡介</h1>
                <h2>設計團隊</h2>
                <p>負責所有設計範疇、包括規劃，設計概念、設計可行性之平衡、設計發展及深化</p>
                <h2>項目管理團隊</h2>
                <p>負責所有工程的項目管理服務，包括有關費用的預算及控制﹑工程進度之跟進、提交所需文件與政府部門及物業管理公司、工程上一切聯絡工作、現場監督 </p>
                
                <span></span>
            </div>
            </div>
            
          </div>

        </div>
        <!-- /about list -->


      </div>
    </div>

    <!-- footer -->
    <div class="gototop"><span></span></div>
    <footer>
      <div class="blockquote"><span></span> / 關於</div>
      <div class="copyright">&copy; 2019 Style Design & Project Ltd.</div>
    </footer>



  </div>

<script src='js/css3-animate-it.js' type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>


</body>
</html>