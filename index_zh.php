<?php
require_once('admin/function/db_connect.php');

$result = $conn->query("SELECT * FROM works ORDER BY id DESC");
if ($result->num_rows > 0) {
  // output data of each row
} else {
  $works = array();
}
$conn->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="zh-hk" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Style Design Production - Home</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="css/style.css" type="text/css" />

  </head>

  <body class="current-home lang-en">

    <div class="bg-container">
      <div class="bg" style="background:url(images/bg.jpg) no-repeat center center; background-size: cover;"></div>
    </div>

    <div id="main-container">

      <!-- header -->
      <header>

        <a id="header-logo" href="/index_zh.php"></a>

        <!-- nav -->
        <nav>
          <div id="menu-options" class="close">
            <span></span>
            <div class="opened">關閉</div>
          <div class="closed">選單</div>
          </div>

          <ul class="close">
            <li><a href="index_zh.php">首頁</a></li>
            <li>
              <a href="work_residential_zh.php">項目</a>
              <div class="sub">
                <a href="work_residential_zh.php">住宅項目</a>
                <a href="work_retails_zh.php">商業項目</a>
                <a href="work_office_zh.php">辦公室項目</a>
              </div>
            </li>
            <li><a href="about_zh.php">關於</a></li>
            <li><a href="team_zh.php">團隊</a></li>
            <li><a href="blog_zh.php">文章</a></li>
            <li><a href="contact_zh.php">聯絡</a></li>
          </ul>

          <div class="lang-options">
            <a href="index.php" class="en">En</a> /
            <a href="index_zh.php" class="chi">中</a>
          </div>
        </nav>


      </header>



      <div class="main-wrapper">
        <div class="main-content">

          <!-- home list -->
          <?php
          $i = 1;
          while ($work = $result->fetch_assoc()) {
            ?>
            <div class="home-list" data-work="p<?php echo $i; ?>">

              <div class="arrow">
                <div class="prev disable"></div>
                <div class="next"></div>
              </div>

              <div class="txtcontent">
                <h1 class="title">
                  <span><?php echo $work['title_zh']; ?></span>
                </h1>
                <p class="description">
                  <span><?php echo nl2br($work['content_zh']); ?></span>
                </p>
                <div class="btn-border">
                  <a href='work_residential_zh.php'>Read More</a>
                </div>
              </div>

              <div class="thumbnail">
                <div class="number"><span> <?php
                    echo str_pad($i, 2, "0", STR_PAD_LEFT);
                    ?></span></div>
                <div class="img">
                  <?php $photo = json_decode($work['photos']); ?>
                  <img src="/photo/work/<?php echo $photo[0]; ?>">
                </div>
                <div class="location">
                  <span><?php echo strtoupper($work['place']); ?></span>
                </div>
              </div>

            </div>
            <!-- /home list -->
            <?php
            $i++;
          }
          ?>

        </div>
      </div>

      <!-- footer -->
      <footer>
        <div class="blockquote"><span></span> / 首頁</div>
        <div class="copyright">&copy; 2019 Style Design & Project Ltd.</div>
      </footer>



    </div>

    <script src='js/jquery.shuffleLetters.js' type="text/javascript"></script>
    <script src="js/common.js" type="text/javascript"></script>

  </body>

</html>