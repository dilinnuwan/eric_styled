<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="zh-hk" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Style Design Production - About</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/animations.css" type="text/css">

</head>

<body class="current-about lang-en">

  <div id="main-container">

    <!-- header -->
    <header>

      <a id="header-logo" href="/"></a>
      
      <!-- nav -->
      <nav>
        <div id="menu-options" class="close">
          <span></span>
          <div class="opened">Close</div>
          <div class="closed">Menu</div>
        </div>

        <ul class="close">
          <li><a href="index.php">Home</a></li>
          <li>
            <a href="work_residential.php">Works</a>
            <div class="sub">
                <a href="work_residential.php">Residential Project</a>
                <a href="work_retails.php">Retails Project</a>
                <a href="work_office.php">Office Project</a>
            </div>
          </li>
          <li><a href="about.php">About</a></li>
          <li><a href="team.php">Team</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
        </ul>

        <div class="lang-options">
          <a href="about.php" class="en">En</a> / 
          <a href="about_zh.php" class="chi">中</a>
        </div>
      </nav>

    </header>

    <div class="main-wrapper">
      <div class="main-content">

        <!-- about content -->
        <div class="content about-list">

          <div class="lt-fixed">
            <ul class="">
              <li class="current" data-section='s1'><a href="#s1">Background & Service</a></li>
              <li data-section='s2'><a href="#s2">Company Profile</a></li>
              
              <span></span>
            </ul>
          </div>

          <div class="rt">
            <div class="animatedParent animateOnce">
            <div id="s1" class="section animated fadeInUpShort">
                <h1>Background & Service</h1>
                <p>STYLE Design & Project Ltd. was established in 2011 and was founded by two experienced project managers in the design and construction industry. We offer a one-step service to assist our clients in various types of projects. Ranging from planning, design development, project management to fit-out works. Our professional team in related discipline will offer genius solutions which best fit clients’ needs.</p>
                <span></span>
            </div>
            </div>
            <div class="animatedParent animateOnce">
            <div id="s2" class="section animated fadeInUpShort">
                <h1>Company Profile</h1>
                <h2>DESIGN TEAM</h2>
                <p>In charge of all design aspects, which include planning, concept design, design adaptation, design development and detail design.</p>
                <h2>PROJECT TEAM</h2>
                <p>Taking care of all onsite and offsite project management services, budget estimation, program, submission, coordination and site monitoring. </p>
                
                <span></span>
            </div>
            </div>
            
          </div>

        </div>
        <!-- /about list -->


      </div>
    </div>

    <!-- footer -->
    <div class="gototop"><span></span></div>
    <footer>
      <div class="blockquote"><span></span> / About</div>
      <div class="copyright">&copy; 2019 Style Design & Project Ltd.</div>
    </footer>



  </div>

<script src='js/css3-animate-it.js' type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>


</body>
</html>