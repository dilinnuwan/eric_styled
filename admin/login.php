<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="http://www.style-design.com.hk/demo/2018/images/logo.svg">
    <title>Style Design & Project Ltd.</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
    <!--[if lt IE 9]>
            <script src="assets/js/html5shiv.min.js"></script>
            <script src="assets/js/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left">
                <a href="{{ route('home') }}" class="logo">
                    <img src="http://www.style-design.com.hk/demo/2018/images/logo.svg" width="70px" height="50px" alt="">
                </a>
            </div>

            <a id="toggle_btn" href="javascript:void(0);"><i class="la la-bars"></i></a>

            <div class="page-title-box pull-left">
                <h3>Style Design & Project Ltd.</h3>
            </div>

            <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>


        </div>


<div class="main-wrapper">
    <div class="account-page">
        <div class="container">
            <br>
            <h3 class="account-title">Admin Login</h3>
            <div class="account-box">
                <div class="account-wrapper" style="background-color: #0066cc">
                    <div class="account-logo">
                        <a href=""><img src="http://www.style-design.com.hk/demo/2018/images/logo.svg" alt=""></a>
                    </div>
                    <form action="function/auth/auth.php" method='POST'>
                    <input type='hidden' name='action' value='login'>
                        <div class="form-group form-focus">
                            <label class="control-label">Username</label>
                            <input class="form-control floating" type="text" name='username'>
                        </div>
                        <div class="form-group form-focus">
                            <label class="control-label">Password</label>
                            <input class="form-control floating" type="password" name='password'>
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-warning btn-block account-btn" type="submit">Login</button>
                        </div>
                         <?php echo $_GET['msg']==null?"":"<br><h3>*".$_GET['msg']."</h3>";?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("include/footer.php"); ?>