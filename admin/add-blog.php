<?php
$nav = 'blog';
include("include/header.php");
require_once('function/db_connect.php');

$result = $conn->query("SELECT * FROM blogs");
if ($result->num_rows > 0) {
  // output data of each row
} else {
  $works = array();
}
$conn->close();
?>
<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="row">
      <div class="col-xs-8">
        <h4 class="page-title">Blog</h4>
      </div>
      <div class="col-xs-4 text-right m-b-30">
        <a href="#" class="btn btn-primary rounded pull-right" data-toggle="modal" data-target="#add_job"><i class="fa fa-plus"></i> Add Blog</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <?php if ($_GET['msg']) { ?>
          <h4><?php echo $_GET['msg'] ?></h4>
        <?php } ?>
        <div class="table-responsive">
          <table class="table table-striped custom-table m-b-0 datatable">
            <thead>
              <tr>
                <th class="text-center">#</th>
                <th class="text-center">Title</th>
                <th class="text-center">Type</th>
                <th class="text-center">Content</th>
                <th class="text-center">Photo</th>
                <th class="text-center">Date</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php while ($blog = $result->fetch_assoc()) { ?>
                <tr>
                  <td class="text-center"><?php echo $blog['id']; ?></td>
                  <td class="text-center"><?php echo $blog['title']; ?></td>
                  <td class="text-center"><?php if ($blog['type'] == 1) { ?>
                      Residential Project
                    <?php } elseif ($blog['type'] == 2) { ?>
                      Retails Project
                    <?php } elseif ($blog['type'] == 3) { ?>
                      Office Project
                    <?php } ?></td>
                  <td class="text-center"><?php echo $blog['content'] ?></td>
                  <td class="text-center"><?php foreach (json_decode($blog['photos']) as $photo) { ?>
                      <img src="/photo/blog/<?php echo $photo; ?>" width="100px" height="80px" alt="">
                    <?php } ?></td>
                  <td class="text-center"><?php echo $blog['date']; ?></td>
                  <td class="text-center">
                    <div class="dropdown">
                      <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="#" title="Edit" data-toggle="modal" data-target="#edit_job_<?php echo $blog['id']; ?>"><i class="fa fa-pencil m-r-5"></i>
                            Edit</a></li>
                        <li><a href="#" title="Delete" data-toggle="modal" data-target="#delete_job_<?php echo $blog['id']; ?>"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <div id="edit_job_<?php echo $blog['id'] ?>" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Edit Blog</h4>
                    </div>
                    <div class="modal-body">
                      <form action="function/update.php" method="POST" enctype="multipart/form-data">
                        <input name='type' type='hidden' value='blog'>
                        <input name='action' type='hidden' value='edit'>
                        <input name='id' type='hidden' value='<?php echo $blog['id']; ?>'>
                        <input name='item_id' type='hidden' value=''>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Title:</label>
                              <input class="form-control" type="text" name='title' value="<?php echo $blog['title']; ?>" maxlength="25" required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Type:</label>
                              <select class="select" name='type_u'>
                                <option <?php if ($blog['type'] == 1) { ?>selected <?php } ?> value="1">Residential Project</option>
                                <option <?php if ($blog['type'] == 2) { ?>selected <?php } ?>value="2">Retails Project</option>
                                <option <?php if ($blog['type'] == 3) { ?>selected <?php } ?>value="3">Office Project</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Title (中文):</label>
                              <input class="form-control" type="text" name='title_zh' value="<?php echo $blog['title_zh']; ?>" maxlength="25" required>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group form-focus">
                              <label class="control-label">Date</label>
                              <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" value="<?php echo $blog['date']; ?>" name='date' required></div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Content:</label>
                              <textarea class="form-control" rows="10" name='content' required><?php echo $blog['content']; ?></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Content (中文):</label>
                              <textarea class="form-control" rows="10" name='content_zh' required><?php echo $blog['content_zh']; ?></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <?php $photos = json_decode($blog['photos']);
                          $upload_url = "https://techmaster.vn/fileman/Uploads/users/129/upload.png";
                          ?>
                          <div class="col-md-6">
                            <label>Photo 1</label><br>
                            <img id="photo_1_<?php echo $blog['id']; ?>" src="<?php echo $photos[0] == null ? $upload_url : '/photo/blog/' . $photos[0]; ?>" alt="" style="max-width:257px;">
                            <br>
                            <input name="photo_1" type='file' onchange="readURL(this, 'photo_1_<?php echo $blog['id']; ?>');" style="padding:10px;background:#FFFFFF;">
                          </div>
                          <div class="col-md-6">
                            <label>Photo 2</label><br>
                            <img id="photo_2_<?php echo $blog['id']; ?>" src="<?php echo $photos[1] == null ? $upload_url : '/photo/blog/' . $photos[1]; ?>" alt="" style="max-width:257px;">
                            <br>
                            <input name="photo_2" type='file' onchange="readURL(this, 'photo_2_<?php echo $blog['id']; ?>');" style="padding:10px;background:#FFFFFF;">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <label>Photo 3</label><br>
                            <img id="photo_3_<?php echo $blog['id']; ?>" src="<?php echo $photos[2] == null ? $upload_url : '/photo/blog/' . $photos[2]; ?>" alt="" style="max-width:257px;">
                            <br>
                            <input name="photo_3" type='file' onchange="readURL(this, 'photo_3_<?php echo $blog['id']; ?>');" style="padding:10px;background:#FFFFFF;">
                          </div>
                          <div class="col-md-6">
                            <label>Photo 4</label><br>
                            <img id="photo_4_<?php echo $blog['id']; ?>" src="<?php echo $photos[3] == null ? $upload_url : '/photo/blog/' . $photos[3]; ?>" alt="" style="max-width:257px;">
                            <br>
                            <input name="photo_4" type='file' onchange="readURL(this, 'photo_4_<?php echo $blog['id']; ?>');" style="padding:10px;background:#FFFFFF;">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <label>Photo 5</label><br>
                            <img id="photo_5_<?php echo $blog['id']; ?>" src="<?php echo $photos[4] == null ? $upload_url : '/photo/blog/' . $photos[4]; ?>" alt="" style="max-width:257px;">
                            <br>
                            <input name="photo_5" type='file' onchange="readURL(this, 'photo_5_<?php echo $blog['id']; ?>');" style="padding:10px;background:#FFFFFF;">
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">SAVE</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div id="delete_job_<?php echo $blog['id']; ?>" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content modal-md">
                <div class="modal-header">
                  <h4 class="modal-title">Delete Blog</h4>
                </div>
                <form action="function/update.php" method="POST">
                  <input name='type' type='hidden' value='blog'>
                  <input name='action' type='hidden' value='delete'>
                  <input name='id' type='hidden' value='<?php echo $blog['id']; ?>'>
                  <div class="modal-body card-box">
                    <p>Are you sure want to delete？</p>
                    <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                      <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        <?php } ?>
        </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<div id="add_job" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Add Blog</h4>
      </div>
      <div class="modal-body">

        <div> </div>

        <form action="function/update.php" method="POST" enctype="multipart/form-data">
          <input name='type' type='hidden' value='blog'>
          <input name='action' type='hidden' value='create'>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Title</label>
                <input class="form-control" type="text" name='title' maxlength="25" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Type:</label>
                <select class="select" name='type_u'>
                  <option value="1">Residential Project</option>
                  <option value="2">Retails Project</option>
                  <option value="3">Office Project</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Title (中文):</label>
                <input class="form-control" type="text" name='title_zh' value="" maxlength="25" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-focus">
                <label class="control-label">Date</label>
                <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name='date' required></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Content:</label>
                <textarea class="form-control" rows="10" name='content' required></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Content (中文):</label>
                <textarea class="form-control" rows="10" name='content_zh' required></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Photo 1</label><br>
              <img id="photo_1" src="https://techmaster.vn/fileman/Uploads/users/129/upload.png" alt="" style="max-width:257px;">
              <br>
              <input name="photo_1" type='file' onchange="readURL(this, 'photo_1');" style="padding:10px;background:#FFFFFF;">
            </div>
            <div class="col-md-6">
              <label>Photo 2</label><br>
              <img id="photo_2" src="https://techmaster.vn/fileman/Uploads/users/129/upload.png" alt="" style="max-width:257px;">
              <br>
              <input name="photo_2" type='file' onchange="readURL(this, 'photo_2');" style="padding:10px;background:#FFFFFF;">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Photo 3</label><br>
              <img id="photo_3" src="https://techmaster.vn/fileman/Uploads/users/129/upload.png" alt="" style="max-width:257px;">
              <br>
              <input name="photo_3" type='file' onchange="readURL(this, 'photo_3');" style="padding:10px;background:#FFFFFF;">
            </div>
            <div class="col-md-6">
              <label>Photo 4</label><br>
              <img id="photo_4" src="https://techmaster.vn/fileman/Uploads/users/129/upload.png" alt="" style="max-width:257px;">
              <br>
              <input name="photo_4" type='file' onchange="readURL(this, 'photo_4');" style="padding:10px;background:#FFFFFF;">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Photo 5</label><br>
              <img id="photo_5" src="https://techmaster.vn/fileman/Uploads/users/129/upload.png" alt="" style="max-width:257px;">
              <br>
              <input name="photo_5" type='file' onchange="readURL(this, 'photo_5');" style="padding:10px;background:#FFFFFF;">
            </div>
          </div>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <div class="m-t-20 text-center">
            <button class="btn btn-primary">Save & upload</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  function readURL(input, id) {
    var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#' + id).attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    } else {
      $('#' + id).attr('src', 'https://techmaster.vn/fileman/Uploads/users/129/upload.png');
    }
  }
</script>
<?php include("include/footer.php"); ?>